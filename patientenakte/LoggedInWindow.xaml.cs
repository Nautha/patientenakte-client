﻿using patientenakte.Patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace patientenakte {
    /// <summary>
    /// Interaktionslogik für LoggedInWindow.xaml
    /// </summary>
    public partial class LoggedInWindow : Window {

        private DatabaseController databaseController;

        public LoggedInWindow(DatabaseController databaseController) {
            InitializeComponent();
            this.databaseController = databaseController;
        }

        private void _Button_PatientMenu_Click(object sender, RoutedEventArgs e) {
            MessageBox.Show("clicked", "Attention", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            this._FrameLoggedIn.Content = new PatientView(this.databaseController);
            //_FrameLoggedIn.NavigationService.Navigate(new PatientView());
            //frame1.NavigationService.Navigate(new Page1());
        }
    }
}
