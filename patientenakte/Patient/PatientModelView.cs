﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patientenakte.Patient {
    class PatientModelView {

        private List<PatientModel> patients;
        private DatabaseController databaseController;

        public PatientModelView(DatabaseController databaseController) {
            patients = new List<PatientModel>();
            this.databaseController = databaseController;
        }

        public List<PatientModel> loadPatientsFromDatabase() {
            var patientJSON = this.databaseController.getPatients();

            foreach (JContainer jc in patientJSON) {
                this.patients.Add(new PatientModel(
                    jc["healthInsurance"].ToString(),
                    jc["insuranceNumber"].ToString(),
                    jc["firstname"].ToString(),
                    jc["lastname"].ToString(),
                    jc["street"].ToString(),
                    jc["houseNumber"].ToString(),
                    jc["postCode"].ToString(),
                    jc["town"].ToString(),
                    Convert.ToDateTime(jc["birthday"].ToString()),
                    jc["phone"].ToString(),
                    jc["particularities"].ToString()
                    ));
            }

            return this.patients;
        }
    }
}
